package drawonline.muman.com.drawonline.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PointF
import android.graphics.RectF
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import java.util.ArrayList

import drawonline.muman.com.drawonline.data.AppConstants
import drawonline.muman.com.drawonline.ui.model.Line

class DrawingBoard : View {

    private lateinit var mLinePaint: Paint

    var linesToDraw: MutableList<Line> = ArrayList()
    private var currentDrawingLine: Line? = null
    private val newPointValidRect = RectF()

    var lineColor = Color.RED
        get() {
            if (field == 0) {
                return ContextCompat.getColor(context, android.R.color.black)
            } else {
                return field
            }
        }

    var lineWidth = AppConstants.DEFAULT_LINE_WIDTH
        set(value) {
            field = value
            mLinePaint.strokeWidth = value
        }

    var boardListener: BoardListener? = null

    private val MIN_DISTANCE_FROM_PREVOIUS_POINT = 10f

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mLinePaint = Paint()
        mLinePaint.flags = Paint.ANTI_ALIAS_FLAG
        mLinePaint.color = Color.RED
        mLinePaint.strokeWidth = lineWidth
        mLinePaint.style = Paint.Style.STROKE
        mLinePaint.strokeCap = Paint.Cap.ROUND
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        drawLines(canvas)

        if (null != currentDrawingLine) {
            drawLine(currentDrawingLine!!, canvas)
        }
    }

    fun clearAll() {
        linesToDraw.clear()
        invalidate()
    }

    private fun drawLines(canvas: Canvas) {
        for (line in linesToDraw) {
            drawLine(line, canvas)
        }
    }

    private fun drawLine(line: Line, canvas: Canvas) {
        val linePoints = line.points
        val linePath = Path()

        adjustPaintToLineOptions(line)

        var i = 0
        while (i < linePoints.size) {
            val currentPoint = linePoints[i]

            if (0 == i) {
                linePath.moveTo(currentPoint.x, currentPoint.y)
            } else if (linePoints.lastIndex == i) {
                linePath.lineTo(currentPoint.x, currentPoint.y)
            } else {
                linePath.lineTo(currentPoint.x, currentPoint.y)
            }
            ++i
        }

        canvas.drawPath(linePath, mLinePaint)
    }

    private fun adjustPaintToLineOptions(line: Line) {
        mLinePaint.color = line.color
        mLinePaint.strokeWidth = line.width
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val action = event.action
        val x = event.x
        val y = event.y

        if (action == MotionEvent.ACTION_DOWN) {
            currentDrawingLine = Line(ArrayList(), lineColor, lineWidth)
            currentDrawingLine?.points?.add(PointF(x, y))
            invalidate()
            return true
        } else if (action == MotionEvent.ACTION_MOVE) {
            val newPoint = PointF(x, y)
            if (isPointValid(newPoint)) {
                currentDrawingLine!!.points.add(newPoint)
                invalidate()
            }
        } else if (action == MotionEvent.ACTION_UP) {
            val newPoint = PointF(x, y)

            currentDrawingLine?.points?.let {
                if(it.size >= AppConstants.LINE_MIN_POINTS_COUNT) {
                    currentDrawingLine!!.points.add(newPoint)
                    linesToDraw.add(currentDrawingLine!!)
                    boardListener?.onLineAdded(currentDrawingLine!!)
                }
            }

            currentDrawingLine = null
            invalidate()
        }

        return false
    }

    /**
     * Checks if given point is in appropriate distance from the previous drawn point
     *
     * @param point
     * @return
     */
    private fun isPointValid(point: PointF): Boolean {
        var isPointValid = true

        if (null != currentDrawingLine && !currentDrawingLine!!.points.isEmpty()) {
            val lastPoint = currentDrawingLine!!.points[currentDrawingLine!!.points.size - 1]
            newPointValidRect.left = lastPoint.x - MIN_DISTANCE_FROM_PREVOIUS_POINT
            newPointValidRect.right = lastPoint.x + MIN_DISTANCE_FROM_PREVOIUS_POINT
            newPointValidRect.top = lastPoint.y - MIN_DISTANCE_FROM_PREVOIUS_POINT
            newPointValidRect.bottom = lastPoint.y + MIN_DISTANCE_FROM_PREVOIUS_POINT
            isPointValid = !newPointValidRect.contains(point.x, point.y)
        }

        return isPointValid
    }

    interface BoardListener {
        fun onLineAdded(lineAdded: Line)
    }
}
