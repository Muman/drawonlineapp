package drawonline.muman.com.drawonline.ui.model

import android.graphics.Color
import android.graphics.PointF
import java.util.*

data class Line(var points : MutableList<PointF> = ArrayList<PointF>(), var color: Int = Color.BLACK, var width: Float)