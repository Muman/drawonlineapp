package drawonline.muman.com.drawonline.app

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import drawonline.muman.com.drawonline.di.AppComponent
import drawonline.muman.com.drawonline.di.AppInjector
import drawonline.muman.com.drawonline.utils.LogUtils.makeLogTag
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    lateinit var appComponent: AppComponent

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        appComponent = AppInjector.init(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingAndroidInjector
    }

    companion object {
        private val LOG_TAG = makeLogTag(App::class.java)
    }
}
