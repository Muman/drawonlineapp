package drawonline.muman.com.drawonline.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import drawonline.muman.com.drawonline.R
import drawonline.muman.com.drawonline.data.AppConstants.DEFAULT_LINE_COLOR
import drawonline.muman.com.drawonline.ui.dialog.BrushSizePickerDialog
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    val TAG_BRUSH_SIZE_PICKER = "TAG_BRUSH_SIZE_PICKER"

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initUi()
    }

    private fun initUi() {
        ivBack.setOnClickListener { drawingBoard.clearAll() }
        ovalColorSelectionView.color = DEFAULT_LINE_COLOR;
        ovalColorSelectionView.setOnClickListener { showColorPicker() }
        brushSizeSelectionView.setOnClickListener { showBrushSizePicker() }
    }

    private fun showColorPicker() {
        ColorPickerDialogBuilder
                .with(this, R.style.ColorPickerTheme)
                .setTitle(R.string.choose_color)
                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                .density(12)
                .setPositiveButton(android.R.string.ok) { dialog, selectedColor, allColors ->
                    run {
                        drawingBoard.lineColor = selectedColor
                        ovalColorSelectionView.color = selectedColor
                    }
                }
                .setNegativeButton(android.R.string.cancel) { dialog, which -> }
                .initialColor(drawingBoard.lineColor)
                .build()
                .show()
    }

    private fun showBrushSizePicker() {
        var brushSizePickerDialog = supportFragmentManager.findFragmentByTag(TAG_BRUSH_SIZE_PICKER)
        if (null == brushSizePickerDialog) {
            brushSizePickerDialog = BrushSizePickerDialog.newInstance()
            brushSizePickerDialog.show(supportFragmentManager, TAG_BRUSH_SIZE_PICKER)
            brushSizePickerDialog.brushSizePickerListener = brushSizePickerListener
        } else {
            (brushSizePickerDialog as BrushSizePickerDialog).brushSizePickerListener = brushSizePickerListener
        }
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return dispatchingAndroidInjector
    }

    private val brushSizePickerListener = object : BrushSizePickerDialog.BrushSizePickerListener {
        override fun brushSizePicked(brushSize: Int) {
            drawingBoard.lineWidth = brushSize.toFloat()
        }
    }

}
