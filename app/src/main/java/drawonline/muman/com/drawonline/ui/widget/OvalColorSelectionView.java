package drawonline.muman.com.drawonline.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import drawonline.muman.com.drawonline.R;

public class OvalColorSelectionView extends View {
    private int color = Color.WHITE;
    private boolean selected = false;

    private Paint paint;
    private Paint selectionPaint;

    private RectF contentRectf;
    private RectF selectionRectf;
    private int contentWidth;

    public OvalColorSelectionView(Context context) {
        super(context);
        init(null, 0);
    }

    public OvalColorSelectionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public OvalColorSelectionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.OvalColorSelectionView, defStyle, 0);

        color = a.getColor(
                R.styleable.OvalColorSelectionView_content_color,
                color);

        selected = a.getBoolean(R.styleable.OvalColorSelectionView_selected, false);

        a.recycle();

        paint = new Paint();
        paint.setColor(getColor());
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);

        selectionPaint = new Paint();
        selectionPaint.setColor(Color.BLACK);
        selectionPaint.setAntiAlias(true);
        selectionPaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        this.contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;

        float solidColorOffset = paddingLeft + 0.05f * contentWidth;

        if (null == contentRectf) {
            contentRectf = new RectF(paddingLeft + solidColorOffset, paddingTop + solidColorOffset, paddingLeft + contentWidth - solidColorOffset, paddingTop + contentHeight - solidColorOffset);
        } else {
            contentRectf.set(paddingLeft + solidColorOffset, paddingTop + solidColorOffset, paddingLeft + contentWidth - solidColorOffset, paddingTop + contentHeight - solidColorOffset);
        }

        if (null == selectionRectf) {
            selectionRectf = new RectF(paddingLeft, paddingTop, paddingLeft + contentWidth, paddingTop + contentHeight);
        } else {
            selectionRectf.set(paddingLeft, paddingTop, paddingLeft + contentWidth, paddingTop + contentHeight);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(contentWidth/2, contentWidth/2, 50, paint);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        this.selected = selected;
        invalidate();
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
        paint.setColor(getColor());
        invalidate();
    }
}
