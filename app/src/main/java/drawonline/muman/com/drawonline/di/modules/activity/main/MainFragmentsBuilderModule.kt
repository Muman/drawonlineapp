package drawonline.muman.com.drawonline.di.modules.activity.main

import android.support.v7.app.AppCompatActivity

import dagger.Module
import dagger.Provides
import drawonline.muman.com.drawonline.ui.MainActivity

@Module
class MainFragmentsBuilderModule {

    @Provides
    internal fun provideActivity(activity: MainActivity): AppCompatActivity {
        return activity
    }
}
