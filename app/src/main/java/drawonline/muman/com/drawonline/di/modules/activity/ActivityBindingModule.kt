package drawonline.muman.com.drawonline.di.modules.activity

import dagger.Module
import dagger.android.ContributesAndroidInjector
import drawonline.muman.com.drawonline.di.modules.activity.main.MainFragmentsBuilderModule
import drawonline.muman.com.drawonline.di.scopes.PerActivity
import drawonline.muman.com.drawonline.ui.MainActivity

@Module
abstract class ActivityBindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(MainFragmentsBuilderModule::class))
    internal abstract fun contributeMainActivity(): MainActivity
}
