package drawonline.muman.com.drawonline.ui.dialog

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.*
import drawonline.muman.com.drawonline.R
import kotlinx.android.synthetic.main.d_brush_size_picker.*

class BrushSizePickerDialog : BottomSheetDialogFragment() {

    var brushSizePickerListener: BrushSizePickerListener? = null

    companion object {
        fun newInstance(): BrushSizePickerDialog {
            return BrushSizePickerDialog()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.d_brush_size_picker, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        brushSizeSmall.setOnClickListener {
            brushSizePickerListener?.brushSizePicked(resources.getDimensionPixelSize(R.dimen.brush_size_small))
            dismiss()
        }

        brushSizeMedium.setOnClickListener {
            brushSizePickerListener?.brushSizePicked(resources.getDimensionPixelSize(R.dimen.brush_size_medium))
            dismiss()
        }

        brushSizeLarge.setOnClickListener {
            brushSizePickerListener?.brushSizePicked(resources.getDimensionPixelSize(R.dimen.brush_size_large))
            dismiss()
        }
    }

    interface BrushSizePickerListener {
        fun brushSizePicked(brushSize: Int)
    }
}