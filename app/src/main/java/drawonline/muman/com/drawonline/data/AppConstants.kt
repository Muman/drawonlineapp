package drawonline.muman.com.drawonline.data

import android.graphics.Color

object AppConstants {
    const val LINE_MIN_POINTS_COUNT : Int = 1
    const val DEFAULT_LINE_COLOR = Color.RED
    const val DEFAULT_LINE_WIDTH = 20f
}
