package drawonline.muman.com.drawonline.di

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager

import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import drawonline.muman.com.drawonline.app.App

import drawonline.muman.com.drawonline.utils.LogUtils.LOGD
import drawonline.muman.com.drawonline.utils.LogUtils.makeLogTag

class AppInjector private constructor(application: App) {
    companion object {

        private val LOG_TAG = makeLogTag(AppInjector::class.java)

        fun init(application: App): AppComponent {
            val appComponent = DaggerAppComponent.builder()
                    .application(application)
                    .build()
            appComponent.inject(application)
            application.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
                override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                    handleActivity(activity)
                }

                override fun onActivityStarted(activity: Activity) {

                }

                override fun onActivityResumed(activity: Activity) {

                }

                override fun onActivityPaused(activity: Activity) {

                }

                override fun onActivityStopped(activity: Activity) {

                }

                override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {

                }

                override fun onActivityDestroyed(activity: Activity) {

                }
            })
            return appComponent
        }

        private fun handleActivity(activity: Activity) {
            if (activity is HasSupportFragmentInjector) {
                LOGD(LOG_TAG, "Injectable activity: " + activity.javaClass.simpleName + " has been created, performing injection")
                AndroidInjection.inject(activity)
            }
            if (activity is FragmentActivity) {
                activity.supportFragmentManager
                        .registerFragmentLifecycleCallbacks(
                                object : FragmentManager.FragmentLifecycleCallbacks() {
                                    override fun onFragmentAttached(fm: FragmentManager?, f: Fragment?, context: Context?) {
                                        if (f is Injectable) {
                                            LOGD(LOG_TAG, "Injectable fragment:" + f.javaClass.simpleName + " has been created, performing injection")
                                            AndroidSupportInjection.inject(f)
                                        }
                                    }
                                }, true)
            }
        }
    }

}


