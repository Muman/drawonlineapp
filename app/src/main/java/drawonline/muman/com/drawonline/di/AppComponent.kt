package drawonline.muman.com.drawonline.di

import android.app.Application

import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import drawonline.muman.com.drawonline.app.App
import drawonline.muman.com.drawonline.di.modules.AppModule
import drawonline.muman.com.drawonline.di.modules.activity.ActivityBindingModule

@Component(modules = arrayOf(AndroidSupportInjectionModule::class, AppModule::class, ActivityBindingModule::class))
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(Application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}
