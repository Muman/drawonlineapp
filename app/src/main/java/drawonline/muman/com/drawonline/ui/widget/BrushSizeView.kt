package drawonline.muman.com.drawonline.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import drawonline.muman.com.drawonline.R

class BrushSizeView : View {

    var color = Color.BLACK
    private var selected = false
    private var brushRadius: Int = 0

    private lateinit var paint: Paint
    private lateinit var selectionPaint: Paint

    private var contentRectf: RectF? = null
    private var selectionRectf: RectF? = null
    private var contentWidth: Int = 0

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context.obtainStyledAttributes(
                attrs, R.styleable.BrushSizeView, defStyle, 0)

        brushRadius = a.getDimensionPixelSize(
                R.styleable.BrushSizeView_radius,
                20)

        a.recycle()

        paint = Paint()
        paint.color = color
        paint.isAntiAlias = true
        paint.style = Paint.Style.FILL

        selectionPaint = Paint()
        selectionPaint.color = Color.BLACK
        selectionPaint.isAntiAlias = true
        selectionPaint.style = Paint.Style.STROKE
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        val paddingLeft = paddingLeft
        val paddingTop = paddingTop
        val paddingRight = paddingRight
        val paddingBottom = paddingBottom

        this.contentWidth = width - paddingLeft - paddingRight
        val contentHeight = height - paddingTop - paddingBottom

        val solidColorOffset = paddingLeft + 0.05f * contentWidth

        if (null == contentRectf) {
            contentRectf = RectF(paddingLeft + solidColorOffset, paddingTop + solidColorOffset, paddingLeft + contentWidth - solidColorOffset, paddingTop + contentHeight - solidColorOffset)
        } else {
            contentRectf!!.set(paddingLeft + solidColorOffset, paddingTop + solidColorOffset, paddingLeft + contentWidth - solidColorOffset, paddingTop + contentHeight - solidColorOffset)
        }

        if (null == selectionRectf) {
            selectionRectf = RectF(paddingLeft.toFloat(), paddingTop.toFloat(), (paddingLeft + contentWidth).toFloat(), (paddingTop + contentHeight).toFloat())
        } else {
            selectionRectf!!.set(paddingLeft.toFloat(), paddingTop.toFloat(), (paddingLeft + contentWidth).toFloat(), (paddingTop + contentHeight).toFloat())
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawCircle((contentWidth / 2).toFloat(), (contentWidth / 2).toFloat(), brushRadius.toFloat(), paint)
    }

    override fun setSelected(selected: Boolean) {
        super.setSelected(selected)
        this.selected = selected
        invalidate()
    }

    override fun isSelected(): Boolean {
        return selected
    }
}
