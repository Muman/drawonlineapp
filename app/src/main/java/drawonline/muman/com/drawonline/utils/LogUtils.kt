package drawonline.muman.com.drawonline.utils

import android.util.Log

import drawonline.muman.com.drawonline.BuildConfig

object LogUtils {

    private val LOG_PREFIX = "PAINT_"
    private val LOG_PREFIX_LENGTH = LOG_PREFIX.length
    private val MAX_LOG_TAG_LENGTH = 23

    fun makeLogTag(str: String): String {
        return if (str.length > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            LOG_PREFIX + str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1)
        } else LOG_PREFIX + str

    }

    /**
     * Don't use this when obfuscating class names!
     */
    fun makeLogTag(cls: Class<*>): String {
        return makeLogTag(cls.simpleName)
    }

    fun LOGD(tag: String, message: String) {
        if (BuildConfig.DEBUG || Log.isLoggable(tag, Log.DEBUG)) {
            Log.d(tag, message)
        }
    }

    fun LOGV(tag: String, message: String) {
        if (BuildConfig.DEBUG && Log.isLoggable(tag, Log.VERBOSE)) {
            Log.v(tag, message)
        }
    }

    fun LOGI(tag: String, message: String) {
        Log.i(tag, message)
    }


    fun LOGW(tag: String, message: String) {
        Log.w(tag, message)
    }


    fun LOGE(tag: String, message: String) {
        Log.e(tag, message)
    }

    fun LOGE(tag: String, message: String, t: Throwable) {
        Log.e(tag, message, t)
    }
}
